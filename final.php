<?php
if (!file_exists('madeline.php')) {
    copy('https://phar.madelineproto.xyz/madeline.php', 'madeline.php');
}
include 'madeline.php';
$MadelineProto = new \danog\MadelineProto\API('bot.madeline');

$MadelineProto->start();
$MadelineProto->setCallback(function ($update) use ($MadelineProto) {
    //\danog\MadelineProto\Logger::log("Received an update of type ".$update['_']);
    if ($update['_'] == "updateNewMessage"){
        if (mt_rand(0,9) == 6){
            $uid = $update['message']['chat_id'];
            $MadelineProto->messages->forwardMessages([
            'to_peer' => "chat#$uid",
                'id' => [
                    $update['message']['id']
                ]
            ]);
        }
    }
});
$MadelineProto->loop(-1);